package com.robo.server.task;

import com.robo.server.task.scheduled.RoboProcess;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by jianghaoming on 17/9/11.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = {ScheduledApplication.class})
public class RoboProcessSchTest {

    @Autowired
    private RoboProcess process;

    @Test
    public void testProcess(){
        process.checkPythonStatus();
    }
}
