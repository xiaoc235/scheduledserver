package com.robo.server.task.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.Map;

/**
 * Created by conor on 17/8/29.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef="entityManagerFactoryStatistics",
        transactionManagerRef="transactionManagerStatistics",
        basePackages= { "com.robo.server.task.repository", "com.robo.server.task.dao"}) //设置Repository所在位置
public class StatisticsDataSourceConfig {

    @Autowired
    @Qualifier("statisticsDataSource")
    private DataSource statisticsDataSource;

    @Autowired
    private JpaProperties jpaProperties;

    @Bean(name = "entityManagerStatistics")
    @Primary
    public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
        return entityManagerFactoryStatistics(builder).getObject().createEntityManager();
    }

    @Primary
    @Bean(name = "entityManagerFactoryStatistics")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryStatistics(EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(statisticsDataSource)
                .properties(getVendorProperties(statisticsDataSource))
                .packages("com.robo.server.task.entity") //设置实体类所在位置
                .persistenceUnit("statisticsPersistenceUnit")
                .build();
    }



    private Map<String, String> getVendorProperties(DataSource dataSource) {
        return jpaProperties.getHibernateProperties(dataSource);
    }

    @Bean(name = "transactionManagerStatistics")
    @Primary
    PlatformTransactionManager transactionManagerStatistics(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactoryStatistics(builder).getObject());
    }

}
