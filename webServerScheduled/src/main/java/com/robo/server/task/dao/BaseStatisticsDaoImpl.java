package com.robo.server.task.dao;

import com.common.base.model.MyPageResult;
import com.robo.server.web.dao.BaseDaoImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;

/**
 * Created by jianghaoming on 2017/3/1523:08.
 */

@Transactional
@Repository
@Qualifier("BaseDao")
public abstract class BaseStatisticsDaoImpl<T> extends BaseDaoImpl<T> {

    private static final Logger _logger = LoggerFactory.getLogger(BaseStatisticsDaoImpl.class);

    @PersistenceContext(unitName = "statisticsPersistenceUnit")
    EntityManager entityManager;

    @Override
    public void setEntityManager(EntityManager _entityManager) {
        super.setEntityManager(_entityManager);
    }

    @Override
    protected List<T> queryListEntity(String sql, Map<String, Object> params, Class<T> clazz) {
        setEntityManager(entityManager);
        return super.queryListEntity(sql, params, clazz);
    }

    @Override
    protected T queryEntity(String sql, Map<String, Object> params, Class<T> clazz) {
        setEntityManager(entityManager);
        return super.queryEntity(sql, params, clazz);
    }

    @Override
    protected MyPageResult<T> queryListEntityByPage(String sql, Map<String, Object> params, Class clazz, Pageable pageable) {
        setEntityManager(entityManager);
        return super.queryListEntityByPage(sql, params, clazz, pageable);
    }

    @Override
    protected Integer getCountBy(String sql, Map<String, Object> params) {
        setEntityManager(entityManager);
        return super.getCountBy(sql, params);
    }

    @Override
    protected Map<String, Object> getSingleResult(String sql, Map<String, Object> params) {
        setEntityManager(entityManager);
        return super.getSingleResult(sql, params);
    }

    @Override
    protected Integer execute(String sql, Map<String, Object> params) {
        setEntityManager(entityManager);
        return super.execute(sql, params);
    }
}

