package com.robo.server.task.entity.statistics;

import com.robo.server.task.entity.BaseTimeDto;

import javax.persistence.*;

/**
 * 群组统计
 * Created by conor on 2017/8/29.
 */
@Entity
@Table(name = "t_statistics_group_info")
public class StatisticsGroupInfoDto extends BaseTimeDto {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String groupId;//群组Id
    private Integer personTotal;//当前群组总成员数
    private Integer joinCount;//当日群组新增成员数
    private Integer leaveCount;//当日群组离开成员数
    private Integer divingCount;//当日群组潜水成员数
    private Integer activeCount;//当日群组活跃成员数
    private Integer activeDaysTotal;//当前群组总活跃天数
    private Integer activeDaysContinue;//当前群组连续活跃天数
    private Integer messageTotal;//当日消息总数量
    private String currentDay;//当前日期

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getPersonTotal() {
        return personTotal;
    }

    public void setPersonTotal(Integer personTotal) {
        this.personTotal = personTotal;
    }

    public Integer getJoinCount() {
        return joinCount;
    }

    public void setJoinCount(Integer joinCount) {
        this.joinCount = joinCount;
    }

    public Integer getLeaveCount() {
        return leaveCount;
    }

    public void setLeaveCount(Integer leaveCount) {
        this.leaveCount = leaveCount;
    }

    public Integer getDivingCount() {
        return divingCount;
    }

    public void setDivingCount(Integer divingCount) {
        this.divingCount = divingCount;
    }

    public Integer getActiveCount() {
        return activeCount;
    }

    public void setActiveCount(Integer activeCount) {
        this.activeCount = activeCount;
    }

    public Integer getActiveDaysTotal() {
        return activeDaysTotal;
    }

    public void setActiveDaysTotal(Integer activeDaysTotal) {
        this.activeDaysTotal = activeDaysTotal;
    }

    public Integer getActiveDaysContinue() {
        return activeDaysContinue;
    }

    public void setActiveDaysContinue(Integer activeDaysContinue) {
        this.activeDaysContinue = activeDaysContinue;
    }

    public String getCurrentDay() {
        return currentDay;
    }

    public void setCurrentDay(String currentDay) {
        this.currentDay = currentDay;
    }

    public Integer getMessageTotal() {
        return messageTotal;
    }

    public void setMessageTotal(Integer messageTotal) {
        this.messageTotal = messageTotal;
    }
}



