package com.robo.server.task.entity.statistics.response;

import com.common.base.BaseDto;

/**
 * 数据统计分析时间范围
 * Created by conor on 2017/8/17.
 */
public class StatisticsAnalyseTimeResponseDto extends BaseDto {

    private String groupName;///群组名称
    private String analyStartTime;//起始分析时间
    private String analyEndTime;//结束分析时间

    public String getAnalyStartTime() {
        return analyStartTime;
    }

    public void setAnalyStartTime(String analyStartTime) {
        this.analyStartTime = analyStartTime;
    }

    public String getAnalyEndTime() {
        return analyEndTime;
    }

    public void setAnalyEndTime(String analyEndTime) {
        this.analyEndTime = analyEndTime;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}



