package com.robo.server.task.entity.statistics.response;

import com.common.base.BaseDto;

/**
 * 聊天消息类型数量及占比分析
 * Created by conor on 2017/8/17.
 */
public class StatisticsMessageAnalyseResponseDto extends BaseDto {

    private Integer messageTotal;//消息总数量
    private Integer textCount;//文本消息数量
    private Integer emojCount;//表情消息数量
    private Integer articleCount;//文章消息数量
    private Integer picCount;//图片消息数量
    private Integer rewardCount;//红包消息数量
    private Integer videoCount;//视频消息数量
    private Integer voiceCount;//语音消息数量
    private Integer otherCount;//其他消息数量
    private String textRate;//消文本息数量占比（百分比）
    private String emojRate;//表情数量占比（百分比）
    private String articleRate;//文章数量占比（百分比）
    private String picRate;//图片数量占比（百分比）
    private String rewardRate;//红包数量占比（百分比）
    private String videoRate;//视频数量占比（百分比）
    private String voiceRate;//语音数量占比（百分比）
    private String otherRate;//其他数量占比（百分比）

    public Integer getMessageTotal() {
        return messageTotal;
    }

    public void setMessageTotal(Integer messageTotal) {
        this.messageTotal = messageTotal;
    }

    public Integer getTextCount() {
        return textCount;
    }

    public void setTextCount(Integer textCount) {
        this.textCount = textCount;
    }

    public Integer getEmojCount() {
        return emojCount;
    }

    public void setEmojCount(Integer emojCount) {
        this.emojCount = emojCount;
    }

    public Integer getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(Integer articleCount) {
        this.articleCount = articleCount;
    }

    public Integer getPicCount() {
        return picCount;
    }

    public void setPicCount(Integer picCount) {
        this.picCount = picCount;
    }

    public Integer getRewardCount() {
        return rewardCount;
    }

    public void setRewardCount(Integer rewardCount) {
        this.rewardCount = rewardCount;
    }

    public Integer getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(Integer videoCount) {
        this.videoCount = videoCount;
    }

    public Integer getVoiceCount() {
        return voiceCount;
    }

    public void setVoiceCount(Integer voiceCount) {
        this.voiceCount = voiceCount;
    }

    public Integer getOtherCount() {
        return otherCount;
    }

    public void setOtherCount(Integer otherCount) {
        this.otherCount = otherCount;
    }

    public String getTextRate() {
        return textRate;
    }

    public void setTextRate(String textRate) {
        this.textRate = textRate;
    }

    public String getEmojRate() {
        return emojRate;
    }

    public void setEmojRate(String emojRate) {
        this.emojRate = emojRate;
    }

    public String getArticleRate() {
        return articleRate;
    }

    public void setArticleRate(String articleRate) {
        this.articleRate = articleRate;
    }

    public String getPicRate() {
        return picRate;
    }

    public void setPicRate(String picRate) {
        this.picRate = picRate;
    }

    public String getRewardRate() {
        return rewardRate;
    }

    public void setRewardRate(String rewardRate) {
        this.rewardRate = rewardRate;
    }

    public String getVideoRate() {
        return videoRate;
    }

    public void setVideoRate(String videoRate) {
        this.videoRate = videoRate;
    }

    public String getVoiceRate() {
        return voiceRate;
    }

    public void setVoiceRate(String voiceRate) {
        this.voiceRate = voiceRate;
    }

    public String getOtherRate() {
        return otherRate;
    }

    public void setOtherRate(String otherRate) {
        this.otherRate = otherRate;
    }
}



