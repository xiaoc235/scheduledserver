package com.robo.server.task.entity.statistics.response;

import com.common.base.BaseDto;

/**
 * 用户成员数量及相关占比分析
 * Created by conor on 2017/8/17.
 */
public class StatisticsPersonAnalyseResponseDto extends BaseDto {

    private Integer personCurTotal;//当前总用户数
    private Integer personActiveCount;//活跃用户数
    private Integer personDivingCount;//潜水用户数
    private Integer personOtherCount;//其他用户数（当前总用户数-活跃用户数-潜水用户数）
    private String personActiveRate;//活跃用户数占比（百分比）
    private String personDivingRate;//潜水用户数占比（百分比）
    private String personOtherRate;//其他用户数占比（百分比）

    public Integer getPersonCurTotal() {
        return personCurTotal;
    }

    public void setPersonCurTotal(Integer personCurTotal) {
        this.personCurTotal = personCurTotal;
    }

    public Integer getPersonActiveCount() {
        return personActiveCount;
    }

    public void setPersonActiveCount(Integer personActiveCount) {
        this.personActiveCount = personActiveCount;
    }

    public Integer getPersonDivingCount() {
        return personDivingCount;
    }

    public void setPersonDivingCount(Integer personDivingCount) {
        this.personDivingCount = personDivingCount;
    }

    public Integer getPersonOtherCount() {
        return personOtherCount;
    }

    public void setPersonOtherCount(Integer personOtherCount) {
        this.personOtherCount = personOtherCount;
    }

    public String getPersonActiveRate() {
        return personActiveRate;
    }

    public void setPersonActiveRate(String personActiveRate) {
        this.personActiveRate = personActiveRate;
    }

    public String getPersonDivingRate() {
        return personDivingRate;
    }

    public void setPersonDivingRate(String personDivingRate) {
        this.personDivingRate = personDivingRate;
    }

    public String getPersonOtherRate() {
        return personOtherRate;
    }

    public void setPersonOtherRate(String personOtherRate) {
        this.personOtherRate = personOtherRate;
    }
}



