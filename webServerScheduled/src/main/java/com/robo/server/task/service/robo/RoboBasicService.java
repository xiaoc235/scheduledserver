package com.robo.server.task.service.robo;

import com.common.base.exception.BusinessException;
import com.robo.server.web.entity.robo.RoboBasicInfoDto;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jianghaoming on 17/9/11.
 */
@Service
public interface RoboBasicService {


    public List<RoboBasicInfoDto> getPidListByStatus() throws DataAccessException, BusinessException, Exception;

    public RoboBasicInfoDto updateStatusById(String id, String status)  throws DataAccessException, BusinessException, Exception;
}
