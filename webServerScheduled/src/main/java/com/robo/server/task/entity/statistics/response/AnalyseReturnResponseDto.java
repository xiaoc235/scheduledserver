package com.robo.server.task.entity.statistics.response;

import com.common.base.BaseDto;

/**
 * Created by conor on 2017/8/17.
 */
public class AnalyseReturnResponseDto extends BaseDto {

    private String name;//名称
    private Integer value;//数量
    private String rate;//占比

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
}



