package com.robo.server.task.em;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 群组成员活跃状态
 * 平均每天消息数量  溺水=0  1<=潜水<=3     4<=较活跃<=10   11<=很活跃
 */
public enum PersonActiveStatusEnum {
    drowning("0"),
    diving_bottom("1"),
    diving_top("3"),
    active_bottom("4"),
    active_top("10"),
    active_highly_bottom("11");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(drowning.getValue(),"溺水");
        desc.put(diving_bottom.getValue(),"潜水（平均每日最小消息数量）");
        desc.put(diving_top.getValue(),"潜水（平均每日最大消息数量）");
        desc.put(active_bottom.getValue(),"较活跃（平均每日最小消息数量）");
        desc.put(active_top.getValue(),"较活跃（平均每日最大消息数量）");
        desc.put(active_highly_bottom.getValue(),"很活跃（平均每日最小消息数量）");
    }

    //构造函数，枚举类型只能为私有
    private PersonActiveStatusEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(PersonActiveStatusEnum enums) {
        return enums.value;
    }

    public static List<PersonActiveStatusEnum> getAllList() {
        PersonActiveStatusEnum[] types = PersonActiveStatusEnum.values();
        List<PersonActiveStatusEnum> result = new ArrayList<PersonActiveStatusEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
