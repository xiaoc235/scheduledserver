package com.robo.server.task.em;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 群组活跃状态
 * 平均每天消息数量 0<=不活跃<=5     6<=较活跃<=20   21<=很活跃<=100 101<=火爆
 */
public enum GroupActiveStatusEnum {
    active_no_bottom("0"),
    active_no_top("5"),
    active_bottom("6"),
    active_top("20"),
    active_highly_bottom("21"),
    active_highly_top("100"),
    active_very_bottom("101");

    //定义私有变量
    private  String  value ;

    private static Map<String, String> desc = new HashMap<String, String>();
    static {
        desc.put(active_no_bottom.getValue(),"不活跃（平均每日最小消息数量）");
        desc.put(active_no_top.getValue(),"不活跃（平均每日最大消息数量）");
        desc.put(active_bottom.getValue(),"较活跃（平均每日最小消息数量）");
        desc.put(active_top.getValue(),"较活跃（平均每日最大消息数量）");
        desc.put(active_highly_bottom.getValue(),"很活跃（平均每日最小消息数量）");
        desc.put(active_highly_top.getValue(),"很活跃（平均每日最大消息数量）");
        desc.put(active_very_bottom.getValue(),"火爆（平均每日最小消息数量）");
    }

    //构造函数，枚举类型只能为私有
    private GroupActiveStatusEnum(final String  _value) {
        this.value = _value;
    }
    public String getValue() {
        return value;
    }
    public Integer getIntValue() {
        return Integer.parseInt(value);
    }

    public static String getValue(GroupActiveStatusEnum enums) {
        return enums.value;
    }

    public static List<GroupActiveStatusEnum> getAllList() {
        GroupActiveStatusEnum[] types = GroupActiveStatusEnum.values();
        List<GroupActiveStatusEnum> result = new ArrayList<GroupActiveStatusEnum>();
        for (int i = 0; i < types.length; i++) {
            result.add(types[i]);
        }
        return result;
    }

    public static Map<String,String> getAllMap(){
        Map<String,String> resultMap = new HashMap<String,String>();
        resultMap.putAll(desc);
        return resultMap;
    }

    public static String getDesc(final String key) {
        if(desc.containsKey(key)){
            return desc.get(key);
        }else{
            return "";
        }
    }
}
