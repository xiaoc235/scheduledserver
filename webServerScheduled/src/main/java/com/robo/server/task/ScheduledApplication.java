package com.robo.server.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({ "com.robo.server.task.*","com.robo.server.web.*","com.common.redis" })
@EnableAutoConfiguration
public class ScheduledApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ScheduledApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(ScheduledApplication.class);
	}

}
